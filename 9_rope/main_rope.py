import time
from copy import deepcopy  # noqa

with open("input.txt") as fp:
    data = fp.read().split("\n")
    procedure = [(x[0], int(x.split(" ")[-1])) for x in data]


class Position:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __repr__(self):
        return str((self.x, self.y))

    def __eq__(self, other):
        return (isinstance(other, self.__class__)) and (self.x == other.x) and (self.y == other.y)

    def __hash__(self):
        # hash is only here for performance reasons
        # When checking if element E is already in set S, python will compare
        # E only to values of S that have the same hash (constant lookup time)
        # if hash(E)==hash(E'), then E AND E' can be in S as long as E != E'
        # But ofc the less collisions, the better the performance are.
        # However, if they are equal they should ABSOLUTELY have the same hash
        return hash(self.__repr__())


class Rope:
    sqrt_2 = 2 ** (1 / 2)

    def __init__(self, head: list[int] = (0, 0), tail: list[int] = (0, 0)):
        self.head = Position(head[0], head[1])
        self.tail = Position(tail[0], tail[1])
        self.tail_history = [deepcopy(self.tail)]
        if self.get_distance() > self.sqrt_2:
            raise Exception("head and tail cannot be more distant than 1.4142")

    def get_distance(self):
        dist = ((self.head.x - self.tail.x) ** 2 + (self.head.y - self.tail.y) ** 2) ** (1 / 2)
        return dist

    def move(self, direction: str, distance: int):
        for step in range(distance):
            if direction == "R":
                self.head.x += 1
                if self.get_distance() > self.sqrt_2:
                    self.tail.y = self.head.y
                    self.tail.x += 1
            if direction == "L":
                self.head.x -= 1
                if self.get_distance() > self.sqrt_2:
                    self.tail.y = self.head.y
                    self.tail.x -= 1
            if direction == "U":
                self.head.y += 1
                if self.get_distance() > self.sqrt_2:
                    self.tail.x = self.head.x
                    self.tail.y += 1
            if direction == "D":
                self.head.y -= 1
                if self.get_distance() > self.sqrt_2:
                    self.tail.x = self.head.x
                    self.tail.y -= 1
            self.tail_history.append(deepcopy(self.tail))
            if self.get_distance() > self.sqrt_2:
                raise Exception("head and tail can NEVER be more distant than 1.4142")

    def execute_procedure(self, procedure: list[tuple[str, int]]):
        for order in procedure:
            self.move(direction=order[0], distance=order[1])


# answer_1 :  5619
# time_1 :  0.024242292158305645
start = time.monotonic()
r = Rope(head=(0, 0), tail=(0, 0))
r.execute_procedure(procedure)
full_set = set(r.tail_history)
answer_1 = len(full_set)
time_1 = time.monotonic() - start
print("answer_1 : ", answer_1)
print("time_1 : ", time_1)


# # ----- VERIFICATION -----
# answer_set = {
#     Position(0, 0),
#     Position(1, 0),
#     Position(2, 0),
#     Position(3, 0),
#     Position(4, 1),
#     Position(4, 2),
#     Position(1, 2),
#     Position(2, 2),
#     Position(3, 2),
#     Position(4, 2),
#     Position(3, 3),
#     Position(4, 3),
#     Position(2, 4),
#     Position(3, 4),
# }
# print()
# print(len(answer_set))
# print("full_set == answer_set : ", full_set == answer_set)
# print("full_set - answer_set : ", full_set - answer_set)
# print("answer_set - full_set : ", answer_set - full_set)
