import time

import numpy as np

with open("input.txt") as fp:
    data = fp.read().split("\n")
data = [[int(x) for x in row] for row in data]
data_np = np.array(data)

# ------------------------ QUESTION 1 ------------------------


def is_visible_np(row: int, col: int, data: np.ndarray, dataT: np.ndarray) -> bool:
    tree = data[row][col]
    # left to right
    if (data[row][:col].size == 0) or (tree > max(data[row][:col])):
        return True
    if (data[row][col + 1 :].size == 0) or (tree > max(data[row][col + 1 :])):
        return True
    # top to bottom
    if (dataT[col][:row].size == 0) or (tree > max(dataT[col][:row])):
        return True
    if (dataT[col][row + 1 :].size == 0) or (tree > max(dataT[col][row + 1 :])):
        return True
    return False


def is_visible(row: int, col: int, data: list[list[int]], dataT: list[list[int]]) -> bool:
    tree = data[row][col]
    # left to right
    if (data[row][:col] == []) or (tree > max(data[row][:col])):
        return True
    if (data[row][col + 1 :] == []) or (tree > max(data[row][col + 1 :])):
        return True
    # top to bottom
    if (dataT[col][:row] == []) or (tree > max(dataT[col][:row])):
        return True
    if (dataT[col][row + 1 :] == []) or (tree > max(dataT[col][row + 1 :])):
        return True
    return False


# answer_1 PURE :  1832
# time_1 PURE :  0.051843499997630715
start = time.monotonic()
dataT = [[data[col][row] for col in range(len(data[0]))] for row in range(len(data))]
visibles = 0
for row in range(len(data)):
    for col in range(len(data[0])):
        visible = is_visible(row, col, data, dataT)
        visibles += int(visible)
time_1 = time.monotonic() - start
print("answer_1 PURE : ", visibles)
print("time_1 PURE : ", time_1)

# answer_1 NP :  1832
# time_1 NP :  0.13596200000029057
start = time.monotonic()
data_npT = data_np.T
visibles = 0
for row in range(len(data)):
    for col in range(len(data[0])):
        visible = is_visible_np(row, col, data_np, data_npT)
        visibles += int(visible)
time_1 = time.monotonic() - start
print("answer_1 NP : ", visibles)
print("time_1 NP : ", time_1)


# ------------------------ QUESTION 2 ------------------------
def get_viewing_distance(tree_height, viewing_range: list[int]) -> int:
    distance_not_viewable = [
        distance for distance, height in enumerate(viewing_range) if height >= tree_height
    ]
    if distance_not_viewable == []:
        return len(viewing_range)
    else:
        return distance_not_viewable[0] + 1


def get_scenic_score(
    row: int, col: int, data: list[list[int]] = data, dataT: list[list[int]] = dataT
) -> int:
    tree_height = data[row][col]
    # left to right
    distance_lr = get_viewing_distance(tree_height, data[row][:col][::-1])
    distance_rl = get_viewing_distance(tree_height, data[row][col + 1 :])
    # top to bottom
    distance_tb = get_viewing_distance(tree_height, dataT[col][row + 1 :])
    distance_bt = get_viewing_distance(tree_height, dataT[col][:row][::-1])
    return distance_lr * distance_rl * distance_tb * distance_bt


# answer_2 PURE :  1832
# time_2 PURE :  0.12612083402927965
start = time.monotonic()
dataT = [[data[col][row] for col in range(len(data[0]))] for row in range(len(data))]
max_scenic_score = 0
for row in range(len(data)):
    for col in range(len(data[0])):
        scenic_score = get_scenic_score(row, col)
        if scenic_score > max_scenic_score:
            max_scenic_score = scenic_score
            best_place = (row, col)
print(best_place, max_scenic_score)
time_2 = time.monotonic() - start
print("answer_2 PURE : ", visibles)
print("time_2 PURE : ", time_2)
