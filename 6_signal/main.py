import time

with open("input.txt") as fp:
    data = fp.read().strip()

# answer = 1235
# time = 0.0004265420138835907
start = time.monotonic()
for idx in range(len(data) - 4):
    if len(set(data[idx : idx + 4])) == 4:
        answer_1 = idx + 4
        break
time_1 = time.monotonic() - start
print(answer_1)
print(time_1)


# answer = 3051
# time = 0.001883499906398356
start = time.monotonic()
for idx in range(len(data) - 14):
    if len(set(data[idx : idx + 14])) == 14:
        answer_2 = idx + 14
        break
time_2 = time.monotonic() - start
print(answer_2)
print(time_2)
