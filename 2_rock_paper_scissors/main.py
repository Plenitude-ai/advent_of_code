import time
from collections import defaultdict  # noqa

import pandas as pd

# ----- 1 ----- 0.029594916035421193
start = time.monotonic()
with open("strategy_guide.txt") as fp:
    strat = fp.read().split("\n")
strat = pd.DataFrame([x.split(" ") for x in strat], columns=["opponent", "to_play"])


shape_value = {"X": 1, "Y": 2, "Z": 3}
match = {
    "X": {"A": 3, "B": 0, "C": 6},
    "Y": {"A": 6, "B": 3, "C": 0},
    "Z": {"A": 0, "B": 6, "C": 3},
}
match_df = pd.DataFrame.from_dict(match)


def get_score(to_play, opponent):
    score = match[to_play][opponent] + shape_value[to_play]
    return score


strat["score"] = strat.apply(lambda row: get_score(row.to_play, row.opponent), axis=1)
answer_1 = strat.score.sum()
timer_1 = time.monotonic() - start


# ----- 2 ----- 0.052239500102587044
start = time.monotonic()
with open("strategy_guide.txt") as fp:
    strat = fp.read().split("\n")
strat = pd.DataFrame([x.split(" ") for x in strat], columns=["opponent", "strat_sign"])

shape_value = {"X": 1, "Y": 2, "Z": 3}
match = {
    "X": {"A": 3, "B": 0, "C": 6},
    "Y": {"A": 6, "B": 3, "C": 0},
    "Z": {"A": 0, "B": 6, "C": 3},
}

score_to_do = {"X": 0, "Y": 3, "Z": 6}
strat["score_output"] = strat.apply(lambda row: score_to_do[row.strat_sign], axis=1)


letter_to_letter = defaultdict(dict)
for last_letter in match:
    for letter in match[last_letter]:
        score = match[last_letter][letter]
        letter_to_letter[letter][score] = last_letter

strat["shape_to_do"] = strat.apply(
    lambda row: letter_to_letter[row.opponent][row.score_output], axis=1
)
strat["score_letter"] = strat.apply(lambda row: shape_value[row.shape_to_do], axis=1)
answer_2 = strat["score_output"].sum() + strat["score_letter"].sum()
timer_2 = time.monotonic() - start

print(f"Answer 1 : {answer_1} (time : {timer_1})")
print(f"Answer 1 : {answer_2} (time : {timer_2})")
