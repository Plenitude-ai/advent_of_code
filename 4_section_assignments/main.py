import os
import time

# answer : 503
# time : 0.0036937500117346644
answer_1 = 0
start = time.monotonic()
with open(os.getcwd() + "/input.txt", "r") as fp:
    for row in map(str.strip, fp):
        sets = []
        for section in row.split(","):
            x, y = tuple(section.split("-"))
            sets.append(set(range(int(x), int(y) + 1)))
        if sets[0].issubset(sets[1]) or sets[0].issuperset(sets[1]):
            answer_1 += 1
time_1 = time.monotonic() - start
print(answer_1)
print(time_1)


# answer : 827
# time : 0.003931917017325759
answer_2 = 0
start = time.monotonic()
with open(os.getcwd() + "/input.txt", "r") as fp:
    for row in map(str.strip, fp):
        sets = []
        for section in row.split(","):
            x, y = tuple(section.split("-"))
            sets.append(set(range(int(x), int(y) + 1)))
        intersection = set.intersection(sets[0], sets[1])
        if len(intersection) != 0:
            answer_2 += 1
time_2 = time.monotonic() - start
print(answer_2)
print(time_2)
