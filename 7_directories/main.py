import operator
import time
from functools import reduce  # noqa

# TODO change the input_test.txt to input.txt
with open("input.txt") as fp:
    cmd_output = fp.read().strip()
line_blocks = cmd_output.split("$ ")
line_blocks = [block.strip() for block in line_blocks if block != ""]


# Buidling the filesystem's tree
tree = {}
current_path = []
for line_block in line_blocks:
    # if move from DIR
    if line_block.startswith("cd "):
        dir = line_block.split("cd ")[-1]
        # If go back a step
        if dir == "..":
            current_path = current_path[:-1]
            continue
        # If move to new dir
        else:
            current_path.append(dir)
            # create the new nested node in the tree
            reduce(operator.getitem, current_path[:-1], tree)[current_path[-1]] = {}
    # if list FILES
    else:
        lines = [line for line in line_block.split("\n") if line != "ls"]
        for line in lines:
            # ignore directories
            if line.startswith("dir"):
                continue
            else:
                # Add the files as the leaves of the tree,
                # with their size as value
                size, file = tuple(line.split(" "))
                reduce(operator.getitem, current_path, tree)[file] = int(size)


# ---------------------------- QUESTION 1 ----------------------------
# answer_1 :  1583951
# time_1 :  0.0021282080560922623


def get_size(tree: dict) -> int:
    """
    Computes the total size of a folder,
    summing up all its files and nested files
    """
    size = 0
    for key in tree:
        if isinstance(tree[key], int):
            size += tree[key]
        else:
            size += get_size(tree[key])
    return size


def get_folders_min(tree: dict, size_max: int = 100_000) -> dict:
    """Returns all the folders that weights less than given threshold,
    along with their size"""
    folders = {}
    for key, value in tree.items():
        if isinstance(value, int):
            continue
        key_size = get_size(value)
        if key_size <= size_max:
            folders[key] = [key_size]
        if isinstance(value, dict):
            sub_folders = get_folders_min(value, size_max=size_max)
            if sub_folders != {}:
                if key in folders:
                    folders[key].append(sub_folders)
                else:
                    folders[key] = sub_folders
    return folders


def get_total_sum(min_sizes: dict):
    """Computes the total size of all the given folders"""
    size = 0
    for value in min_sizes.values():
        if isinstance(value, list):
            for elem in value:
                if isinstance(elem, int):
                    size += elem
                elif isinstance(elem, dict):
                    size += get_total_sum(elem)
        elif isinstance(value, dict):
            size += get_total_sum(value)

    return size


START = time.monotonic()
min_sizes = get_folders_min(tree)
answer_1 = get_total_sum(min_sizes)
time_1 = time.monotonic() - START
print("answer_1 : ", answer_1)
print("time_1 : ", time_1)


# ---------------------------- QUESTION 2 ----------------------------
# answer_2 :  214171
# time_2 :  0.0005138339474797249


def get_folders_bigger(
    tree: dict, size_min: int = 208_860, current_min: int = 10_000_000
) -> tuple[dict, int]:
    """
    Builds a graph with all the folders of at least size_min
    Returns such graph and the size of the smaller one
    """
    folders = {}
    for key in tree:
        if isinstance(tree[key], int):
            continue
        key_size = get_size(tree[key])
        if key_size >= size_min:
            sub_folders, current_min = get_folders_bigger(
                tree[key], size_min=size_min, current_min=current_min
            )
            current_min = min(current_min, key_size)
            # If the folder cannot be split in smaller nodes
            # (that would still be bigger than size_min),
            # we consider its current size
            if sub_folders == {}:
                folders[key] = key_size
            else:
                folders[key] = sub_folders
    return folders, current_min


START = time.monotonic()
size_min = get_size(tree["/"]) + 30e6 - 70e6
_, answer_2 = get_folders_bigger(tree, size_min=size_min)
time_2 = time.monotonic() - START

print("answer_2 : ", answer_2)
print("time_2 : ", time_2)
