import os
import string
import time
from itertools import zip_longest  # noqa

# answer_1 :  7889
# 0.000714499969035387
start = time.monotonic()
letter_to_score = {letter: score + 1 for score, letter in enumerate(string.ascii_letters)}
answer_1 = 0
with open(os.getcwd() + "/input.txt", "r") as fp:
    for row in map(str.strip, fp):
        sac1 = row[: len(row) // 2]
        sac2 = row[len(row) // 2 :]
        letter = [i for i in sac2 if i in sac1][0]
        score = letter_to_score[letter]
        answer_1 += score
print("answer_1 : ", answer_1)
time_1 = time.monotonic() - start
print(time_1)

# answer_2 :  2825
# 0.000348874949850142
start = time.monotonic()
letter_to_score = {letter: score + 1 for score, letter in enumerate(string.ascii_letters)}
answer_2 = 0
with open(os.getcwd() + "/input.txt", "r") as fp:
    rows_iterator = map(str.strip, fp)
    all_iterators = [rows_iterator] * 3
    chunks = zip_longest(*all_iterators)
    for row1, row2, row3 in chunks:
        letter = [letter1 for letter1 in row1 if (letter1 in row2) and (letter1 in row3)][0]
        score = letter_to_score[letter]
        answer_2 += score
print("answer_2 : ", answer_2)
time_2 = time.monotonic() - start
print(time_2)
