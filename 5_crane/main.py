import time

# answer : CVCWCRTVQ
# time : 0.0013529169373214245
start = time.monotonic()
with open("procedure.txt") as fp:
    procedures = list(map(str.strip, fp))
procedures = [x.split("move ")[-1].split(" from ") for x in procedures]
procedures = [[int(x[0])] + list(map(int, (x[1].split(" to ")))) for x in procedures]

with open("input.txt") as fp:
    txt = fp.read().split("\n")
data = [[txt[row][idx] for row in range(7, -1, -1)] for idx in range(1, 37, 4)]
data = [[e for e in row if e != " "] for row in data]

for proc in procedures:
    nb = proc[0]
    origin = proc[1] - 1
    dest = proc[2] - 1

    to_move = data[origin][-nb:]

    data[dest] += to_move[::-1]
    data[origin] = data[origin][:-nb]

answer_1 = [row[-1] for row in data]
answer_1 = "".join(answer_1)
time_1 = time.monotonic() - start
print(answer_1)
print(time_1)


# answer : CNSCZWLVT
# time : 0.0009405000600963831
start = time.monotonic()
with open("procedure.txt") as fp:
    procedures = list(map(str.strip, fp))
procedures = [x.split("move ")[-1].split(" from ") for x in procedures]
procedures = [[int(x[0])] + list(map(int, (x[1].split(" to ")))) for x in procedures]

with open("input.txt") as fp:
    txt = fp.read().split("\n")
data = [[txt[row][idx] for row in range(7, -1, -1)] for idx in range(1, 37, 4)]
data = [[e for e in row if e != " "] for row in data]

for proc in procedures:
    nb = proc[0]
    origin = proc[1] - 1
    dest = proc[2] - 1

    to_move = data[origin][-nb:]

    # the new crane can now transfer all the crates at once
    data[dest] += to_move[::+1]
    data[origin] = data[origin][:-nb]

answer_2 = [row[-1] for row in data]
answer_2 = "".join(answer_2)
time_2 = time.monotonic() - start
print(answer_2)
print(time_2)
